const fs = require('fs');
const papaparse = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/chirumani/IPL/src/data/matches.csv';


fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = papaparse.parse(data, {
        header: true, // Treat the first row as headers
    });

    const matchesWonTeamYear = function (parsedData) {
        let years = parsedData.data.map((matchDetails) => matchDetails.season);

        let matchesWonPerTeamPerYear = years.reduce((accumulator, year) => {
            let matchesWonPerTeam = parsedData.data.reduce((teamAccumulator, match) => {
                if (match.season == year) {
                    winner = match.winner;
                    if (teamAccumulator[winner] == undefined) {
                        teamAccumulator[winner] = 1;
                    }
                    else {
                        teamAccumulator[winner] += 1;
                    }
                }
                return teamAccumulator;
            }, {});
            accumulator[year] = matchesWonPerTeam;
            return accumulator;
        }, {});
        return matchesWonPerTeamPerYear;
    };

    //Convert to JSON
    const jsonData = JSON.stringify(matchesWonTeamYear(parsedData));

    // Write the JSON data to a new file
    fs.writeFile('/home/chirumani/IPL/src/public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as 1-matches-per-year.json');
    });
});