const fs = require('fs');
const papaparse = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/chirumani/IPL/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading the CSV file:', err);
    return;
  }

  // Parse the CSV data
  const parsedData = papaparse.parse(data, {
    header: true, // Treat the first row as headers
  });

  let wonBothTossAndMatches = function (parsedData) {
    let wonTossAndMatches=parsedData.data.reduce((accumulator,match)=>{
      if(match['toss_winner']==match['winner']){
        winner=match['winner'];
        if(accumulator[winner]==undefined){
          accumulator[winner]=1;
        }
        else{
          accumulator[winner] += 1;
        }
      }
      return accumulator;
    },{})
    return wonTossAndMatches; 
  }
  const jsonData = JSON.stringify(wonBothTossAndMatches(parsedData));

  //Write the JSON data to a new file
  fs.writeFile('/home/chirumani/IPL/src/public/output/5-won-both-matches-and-toss.json', jsonData, 'utf8', (err) => {
    if (err) {
      console.error('Error writing JSON data to file:', err);
      return;
    }
    console.log('CSV data converted to JSON and saved as 5-won-both-matches-and-toss.json');
  });
});