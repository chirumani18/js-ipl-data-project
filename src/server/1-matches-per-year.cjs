// Require the necessary modules
const fs = require('fs');
const papaparse = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/chirumani/IPL/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading the CSV file:', err);
    return;
  }

  // Parse the CSV data
  const parsedData = papaparse.parse(data, {
    header: true, // Treat the first row as headers
  });

  let matchesInYear = function (parsedData) {
    const years = parsedData.data.map((matchDetails) => matchDetails.season);

    const matchesPlayedInEveryYear = years.reduce((acc, year) => {
      if (acc[year] == undefined) {
        acc[year] = 1;
      }
      else {
        acc[year] += 1;
      }
      return acc;
    }, {})

    return matchesPlayedInEveryYear;
  }

  const jsonData = JSON.stringify(matchesInYear(parsedData));

  // Write the JSON data to a new file
  fs.writeFile('/home/chirumani/IPL/src/public/output/1-matches-per-year.json', jsonData, 'utf8', (err) => {
    if (err) {
      console.error('Error writing JSON data to file:', err);
      return;
    }
    console.log('CSV data converted to JSON and saved as 1-matches-per-year.json');
  });
});