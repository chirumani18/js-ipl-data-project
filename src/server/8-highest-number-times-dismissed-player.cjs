const fs = require('fs');
const Papa = require('papaparse');
const deliveriesFilePath = '/home/chirumani/IPL/src/data/deliveries.csv';
fs.readFile(deliveriesFilePath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading the CSV file:', err);
    return;
  }
  const parsedData = Papa.parse(data, {
    header: true,
  });
  function highestDismissalsBatsman(parsedData) {
    const dismissals = parsedData.data.reduce((accumulator, wicket) => {
      const batsman = wicket['player_dismissed'];
      const bowler = wicket['bowler'];
      const wicketType = wicket['dismissal_kind']

      if (wicketType !== "run out" && batsman !== "") {
        if (accumulator[batsman] == undefined) {
          accumulator[batsman] = {};
        }

        if (accumulator[batsman][bowler] == undefined) {
          accumulator[batsman][bowler] = 1;
        } else {
          accumulator[batsman][bowler]++;
        }
      }
      return accumulator
    }, {});
    const highestNumberOfDismissals = Object.entries(dismissals).reduce((accumulator, [batsman, stats]) => {
      const bowlerWithMaximumDismissals = Object.entries(stats).reduce((maximumDismissals, [bowler, count]) => {
        if (count > maximumDismissals.count) {
          return { bowler, count };
        } else {
          return maximumDismissals;
        }
      }, { bowler: '', count: 0 });
      accumulator[batsman] = { bowler: bowlerWithMaximumDismissals.bowler, count: bowlerWithMaximumDismissals.count };
      return accumulator
    }, {});

    const sortDismissals = Object.entries(highestNumberOfDismissals).sort((before, present) => present[1].count - before[1].count);
    return sortDismissals[0];
  }
  let result = highestDismissalsBatsman(parsedData)
  const jsonData = JSON.stringify(result);
  fs.writeFile('/home/chirumani/IPL/src/public/output/8-highest-number-times-dismissed-player.json', jsonData, 'utf8', (err) => {
    if (err) {
      console.error('Error writing JSON data to file:', err);
      return;
    }
    console.log('CSV data converted to JSON and saved as 8-highest-number-times-dismissed-player.json');
  });
});
