const fs = require('fs');
const papaparse = require('papaparse');
const deliveriesFilePath = '/home/chirumani/IPL/src/data/deliveries.csv';
const matchesFilePath = '/home/chirumani/IPL/src/data/matches.csv';
fs.readFile(matchesFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }
    const matchesParsedData = papaparse.parse(data, {
        header: true,
    });
    fs.readFile(deliveriesFilePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
        const deliveriesParsedData = papaparse.parse(data, {
            header: true,
        });
        function top10EconomyBowlersIn2015(matchesParsedData, deliveriesParsedData) {
            const allMatchesIn2015 = matchesParsedData.data.filter((match) => match['season'] == "2015");
            const idsList = allMatchesIn2015.map((match) => match['id']).sort()
            const minimumId = idsList[0];
            const maximumId = idsList[idsList.length - 1];
            const bowlersData = deliveriesParsedData.data.reduce((accumulator, delivery) => {
                const matchId = parseInt(delivery['match_id']);
                if (matchId >= parseInt(minimumId) && matchId <= parseInt(maximumId)) {
                    const bowler = (delivery['bowler']);
                    if (accumulator[bowler] == undefined) {
                        accumulator[bowler] = { bowlerRuns: 0, bowlerBalls: 0 };
                    }

                    if (delivery['wide_runs'] === "0" && delivery['noball_runs'] === "0") {
                        accumulator[bowler]['bowlerRuns'] += (parseInt(delivery['total_runs']) - parseInt(delivery['extra_runs']));
                        accumulator[bowler]['bowlerBalls']++;
                    }
                }
                return accumulator
            }, {});
            let bowlers=Object.keys(bowlersData).reduce((accumulator,bowler)=>{
                const bowlerEconomy=(bowlersData[bowler]['bowlerRuns']/bowlersData[bowler]['bowlerBalls'])*6;
                
                accumulator[bowler]={
                    ...bowlersData[bowler],
                    economy:bowlerEconomy.toFixed(2),
                };
                return accumulator;
            },{})
            const sortedEconomyBowlers = Object.entries(bowlers).sort(
                (before, present) => before[1].economy - present[1].economy
            );
            
            const top10EconomyBowlers = sortedEconomyBowlers.slice(0, 10).map((stats) => ({
                bowlerName: stats[0],
                bowlerEconomy: stats[1].economy,
            }));

            return top10EconomyBowlers;
        }
        top10EconomyBowlersIn2015(matchesParsedData, deliveriesParsedData);



        //Convert to JSON
        const jsonData = JSON.stringify(top10EconomyBowlersIn2015(matchesParsedData, deliveriesParsedData));

        // Write the JSON data to a new file
        fs.writeFile('/home/chirumani/IPL/src/public/output/4-top-10-economical-bowlers.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
                return;
            }
            console.log('CSV data converted to JSON and saved as src4-top-10-economical-bowlers.json');

        });
     });
});
