const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/chirumani/IPL/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, // Treat the first row as headers
    });

    let playerOfTheMatchForEveryYear = function (parsedData) {
        let years = parsedData.data.reduce((accumulator, match) => {
            const season = match.season

            if (!accumulator.includes(season) && season != undefined) {
                accumulator.push(season)
            }

            return accumulator

        }, [])

        let MostManOfTheMatch = years.reduce((accumulator, year) => {
            let manOfTheMatch = parsedData.data.reduce((teamAccumulator, match) => {
                if (match['season'] == year) {
                    let playerOfTheMatch = match['player_of_match'];
                    if (teamAccumulator[playerOfTheMatch] == undefined) {
                        teamAccumulator[playerOfTheMatch] = 1;
                    }
                    else {
                        teamAccumulator[playerOfTheMatch] += 1;
                    }
                }
                return teamAccumulator;
            }, {})
            let resultArray = Object.entries(manOfTheMatch);

            resultArray.sort((before, present) => present[1] - before[1]);
            const sortedResultArray = Object.fromEntries(resultArray);
            accumulator[year] = Object.keys(sortedResultArray)[0];

            return accumulator;
        }, {})
        return MostManOfTheMatch;
    }

    const jsonData = JSON.stringify(playerOfTheMatchForEveryYear(parsedData));

    fs.writeFile('/home/chirumani/IPL/src/public/output/6-highest-player-of-the-match-award.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as 6-highest-player-of-the-match-award.json');
    });
});