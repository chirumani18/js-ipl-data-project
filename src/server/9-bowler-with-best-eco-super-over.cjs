const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/chirumani/IPL/src/data/deliveries.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, // Treat the first row as headers
    });

    function economicBowlerInSuperOver(parsedData) {
        const superOverBowlers = parsedData.data
            .filter((delivery) => delivery['is_super_over'] !== "0" && delivery['bowler'] !== undefined)
            .reduce((accumulator, delivery) => {
                const bowler = delivery['bowler'];
                if (accumulator[bowler] == undefined) {
                    accumulator[bowler] = 1;
                }
                return accumulator;
            }, {});
        //console.log(Object.keys(superOverBowlers));
        const superOverRuns = Object.keys(superOverBowlers).reduce((accumulator, bowler) => {
            const ballCountAndTotalScore = parsedData.data
                .filter((delivery) => delivery['bowler'] == bowler && delivery['is_super_over'] == "1")
                .reduce((result, delivery) => {
                    if (result['ballCount'] == undefined && result['totalScore'] == undefined) {
                        result['ballCount'] = 0;
                        result['totalScore'] = 0;
                    }
                    const wideRuns = delivery['wide_runs'];
                    const noBallRuns = delivery['noball_runs'];
                    if (parseInt(wideRuns) == 0 && parseInt(noBallRuns) == 0) {
                        result.ballCount += 1;
                    }
                    result.totalScore += parseInt(delivery['total_runs']) - parseInt(delivery['legbye_runs']) - parseInt(delivery['penalty_runs']) - parseInt(delivery['bye_runs']);
                    return result;
                }, {});

            accumulator[bowler] = [ballCountAndTotalScore.totalScore, ballCountAndTotalScore.ballCount];
            return accumulator;
        }, {});
        const economyOfSuperOver = Object.entries(superOverRuns).reduce((accumulator, [superBowler, [totalScore, ballCount]]) => {
            const economy = (totalScore / ballCount) * 6;
            accumulator[superBowler] = economy;
            return accumulator;
        }, {});

        const bowlingAverageArray = Object.entries(economyOfSuperOver).sort((a, b) => a[1] - b[1]);

        return bowlingAverageArray;

    }

    let result = economicBowlerInSuperOver(parsedData)

    const jsonData = JSON.stringify(result[0]);

    // Write the JSON data to a new file
    fs.writeFile('/home/chirumani/IPL/src/public/output/9-bowler-with-best-eco-super-over.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as 9-bowler-with-best-eco-super-over.json');
    });
});


