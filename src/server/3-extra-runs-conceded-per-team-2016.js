// Require the necessary modules
const fs = require('fs');
const papaparse = require('papaparse');

// Read the CSV file
const deliveriesCsvFilePath = '/home/chirumani/IPL/src/data/deliveries.csv';
const matchesCsvFilePath = '/home/chirumani/IPL/src/data/matches.csv';

fs.readFile(deliveriesCsvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const deliveriesParsedData = papaparse.parse(data, {
        header: true, // Treat the first row as headers
    });

    fs.readFile(matchesCsvFilePath, 'utf8', (err, data1) => {
        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
        const matchesParsedData = papaparse.parse(data1, {
            header: true, // Treat the first row as headers
        });

        let extraRunsConcededPerTeamIn2016 = function (deliveriesParsedData, matchesParsedData) {
            const matches2016 = matchesParsedData.data.filter((match) => match['season'] == '2016');
            const matches2016Ids = matches2016.map((match) => match['id']);
            matches2016Ids.sort();
            const minId = matches2016Ids[0];
            const maxId = matches2016Ids[matches2016Ids.length-1];
            console.log(minId,maxId);
            const extraRunsConcededPerTeam = deliveriesParsedData.data.reduce((accumulator, match) => {
                const matchId = parseInt(match['match_id']);
                if (matchId >= minId && matchId <= maxId) {
                  const team = match['bowling_team'];
                  const extraRuns = parseInt(match['extra_runs']);
                  if (accumulator[team]==undefined) {
                    accumulator[team] = extraRuns;
                  } else {
                    accumulator[team] += extraRuns;
                  }
                }
                return accumulator;
              }, {});
              return extraRunsConcededPerTeam;
        }        

        // Convert to JSON
        const jsonData = JSON.stringify(extraRunsConcededPerTeamIn2016(deliveriesParsedData, matchesParsedData));

        // Write the JSON data to a new file
        fs.writeFile('/home/chirumani/IPL/src/public/output/3-extra-runs-conceded-per-team-2016.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
                return;
            }
            console.log('CSV data converted to JSON and saved as 3-extra-runs-conceded-per-team-2016.json');
        });
    });
});
