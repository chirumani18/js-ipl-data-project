const fs = require('fs');
const Papa = require('papaparse');
const csvFilePath = '/home/chirumani/IPL/src/data/matches.csv';
const deliveriesFilePath = '/home/chirumani/IPL/src/data/deliveries.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }
    const matchesParsedData = Papa.parse(data, {
        header: true, 
    });
    fs.readFile(deliveriesFilePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
        const deliveriesParsedData = Papa.parse(data, {
            header: true, 
        });
        let strikerateOfBatsmenEverySeason = function (matchesData, deliveriesData) {
            let years = matchesParsedData.data.reduce((accumulator, match) => {
                const season = match.season
                if (!accumulator.includes(season) && season != undefined) {
                    accumulator.push(season)
                }
                return accumulator
            }, [])
            const matchIdForSeason = years.reduce((accumulator, year) => {
                const yearList = matchesParsedData.data.filter((match) => match['season'] === year)
                    .map((match) => match['id']);
                accumulator[year] = yearList;
                return accumulator
            }, {})
            const toatlRunsByBatsmanSeason = years.reduce((accumulator, year) => {
                accumulator[year] = deliveriesParsedData.data.reduce((result, eachDelivery) => {
                    if (matchIdForSeason[year].includes(eachDelivery['match_id'])) {
                        const batsmanName = eachDelivery['batsman'];
                        const batsmanRuns = parseInt(eachDelivery['batsman_runs']);
                        const wideRuns = eachDelivery['wide_runs'];
                        const noballRuns = eachDelivery['noball_runs'];
                        if (result[batsmanName]==undefined) {
                            
                            result[batsmanName] = { runs: 0, balls: 0, strikeRateBatsman: 0 };
                        }
                        result[batsmanName].runs += batsmanRuns;
                        if (wideRuns === '0' && noballRuns === '0') {
                            result[batsmanName].balls++;
                            
                        }
                        result[batsmanName].strikeRateBatsman = (result[batsmanName].runs / result[batsmanName].balls)*100;
                    }
                    
                    return result;
                    
                }, {});
                return accumulator;
            }, {});
            const strikeRateByBatsman = years.reduce((accumulator, year) => {
                accumulator[year] = {};
                Object.entries(toatlRunsByBatsmanSeason[year]).map(([batsmanName, batsmanData]) => {
                    const {runs, balls,strikeRateBatsman} = batsmanData;
                    accumulator[year][batsmanName] = strikeRateBatsman;
                });
                return accumulator
            }, {});
            return strikeRateByBatsman;
    
        }
        strikerateOfBatsmenEverySeason(matchesParsedData, deliveriesParsedData);
        const jsonData = JSON.stringify(strikerateOfBatsmenEverySeason(matchesParsedData, deliveriesFilePath));
        fs.writeFile('/home/chirumani/IPL/src/public/output/7-strike-rate-of-batsman-each-season.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
                return;
            }
            console.log('CSV data converted to JSON and saved as 7-strike-rate-of-batsman-each-season.json');
        });
       });
});